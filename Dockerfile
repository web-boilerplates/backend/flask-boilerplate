FROM python:3.9-slim
LABEL maintainer="simon@torrentofshame.com"

RUN python3 -m pip install -U pip \
        && groupadd -r backend
        && useradd --no-log-init -r -g backend backend \
        && mkdir -p /home/backend/app \
        && chown -R backend:backend /home/backend

USER backend:backend

WORKDIR /home/backend/app

COPY --chown=backend:backend requirements.txt ./

RUN pip install --no-cache-dir --user -r requirements.txt
ENV PATH="/home/backend/.local/bin:${PATH}"

ENV LOGGING_LOCATION=/var/log/flask-base.log

COPY --chown=backend:backend . .

ENTRYPOINT [ "python3" ]
CMD ["-m", "server", "--host=0.0.0.0", "--port=5000"]
