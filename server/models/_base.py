# -*- coding: utf-8 -*-
"""
    server.models._base
    ~~~~~~~~~~~~~~~~~~~

"""
from server import db
from mongoengine.errors import NotUniqueError, ValidationError
from werkzeug.exceptions import BadRequest, Conflict, NotFound


class BaseDocument(db.Document):

    meta = {
        "abstract": True
    }

    @classmethod
    def createOne(cls, *args, **kwargs):
        _conflict = kwargs.pop("_conflict", None)
        _badrequest = kwargs.pop("_badrequest", None)

        doc = cls(*args, **kwargs)
        try:
            doc.save()
        except NotUniqueError:
            raise Conflict(_conflict)
        except ValidationError:
            raise BadRequest(_badrequest)
        return doc

    @classmethod
    def deleteOne(cls, *args, **kwargs):
        _notfound = kwargs.pop("_notfound", None)

        doc = cls(*args, **kwargs)

        if not doc:
            raise NotFound(_notfound)

        return doc.delete()
