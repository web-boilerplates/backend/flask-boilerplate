# -*- coding: utf-8 -*-
"""
    server.models.tokenblacklist
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
from server import db
from server.models._base import BaseDocument
from datetime import datetime


class TokenBlacklist(BaseDocument):
    jti = db.StringField(max_length=36, required=True)
    created_at = db.DateTimeField(required=True, default=datetime.utcnow)
    revoked = db.BooleanField(default=False, required=True)
    from server.models.user import User
    user = db.ReferenceField(User, required=True)
