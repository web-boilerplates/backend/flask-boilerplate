# -*- coding: utf-8 -*-
"""
    server.models.user
    ~~~~~~~~~~~~~~~~~~

"""
from server import db, bcrypt
from server.models._base import BaseDocument


class User(BaseDocument):
    username = db.StringField(required=True, unique=True)
    password = db.BinaryField(required=True)

    def check_password(self, password: str) -> bool:
        return bcrypt.check_password_hash(self.password, password)

    def __init__(self, *args, **kwargs):
        if (
            kwargs.get("password") is not None
                and isinstance(kwargs.get("password"), str)
        ):
            kwargs["password"] = bcrypt.generate_password_hash(
                kwargs["password"]
            )

        super(User, self).__init__(*args, **kwargs)
