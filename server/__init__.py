# -*- coding: utf-8 -*-
"""
    server.__init__
    ~~~~~~~~~~~~~~~

"""
from os import getenv, environ
from flask import Flask
from flask_mongoengine import MongoEngine
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_cors import CORS
from werkzeug.exceptions import HTTPException
from mongoengine.errors import NotUniqueError

# Init Extensions
db = MongoEngine()
bcrypt = Bcrypt()
jwt = JWTManager()


def create_app():
    """Initialize the App"""
    app = Flask(__name__)

    # Flask Config
    app_settings = getenv("APP_SETTINGS", "server.config.ProductionConfig")
    app.config.from_object(app_settings)

    if app.config.get("DEBUG"):
        environ["FLASK_ENV"] = "development"
        environ["FLASK_DEBUG"] = "1"

    # Setup Extensions
    CORS(app)
    db.init_app(app)
    bcrypt.init_app(app)
    jwt.init_app(app)

    # Override json encoder
    from server.common.json import JSONEncoderBase
    app.json_encoder = JSONEncoderBase

    from server.common.jwt import _init_jwt
    _init_jwt()

    # Register API Blueprint
    from server.api import api_bp
    app.register_blueprint(api_bp, url_prefix="/api")

    # Error Handler
    from server.common.errors import handle_exception
    app.register_error_handler(HTTPException, handle_exception)

    @app.before_first_request
    def _init_server():
        from server.models.user import User
        try:
            usr = User(username="foobar", password="string")
            usr.save()
        except NotUniqueError:
            pass

    return app


app = create_app()
