# -*- coding: utf-8 -*-
"""
    server.__main__
    ~~~~~~~~~~~~~~~

    The main file, allows for easy starting of the backend server.

"""
import os
from flask.cli import FlaskGroup
from server import app
try:
    import pytest
    tests_available = True
except ImportError:
    tests_available = False

os.environ["FLASK_APP"] = "server.__main__:main()"

cli = FlaskGroup(app)


def main():
    return app


@cli.command()
def test():
    """Run Tests"""
    if tests_available:
        pytest.main(["--doctest-modules", "--junitxml=junit/test-results.xml"])
    else:
        app.logger.error(
            "Module PyTest is not installed! "
            "Install dev dependencies before testing!"
        )


if __name__ == "__main__":
    cli()
