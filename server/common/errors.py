# -*- coding: utf-8 -*-
"""
    server.common.errors
    ~~~~~~~~~~~~~~~~~~~~

"""
from flask import json


def handle_exception(e):
    """Give JSON response for HTTP errors."""

    res = e.get_response()

    res.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description
    })

    res.content_type = "application/json"

    return res
