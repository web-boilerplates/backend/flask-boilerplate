# -*- coding: utf-8 -*-
"""
    server.common.jwt
    ~~~~~~~~~~~~~~~~~

"""
from server import jwt
from server.models import (
    User,
    TokenBlacklist
)


def _init_jwt():

    @jwt.user_identity_loader
    def user_identity_loader(user: User):
        return user.id

    @jwt.user_lookup_loader
    def user_lookup_callback(_jwt_header, jwt_data) -> User:
        identity = jwt_data["sub"]
        return User.objects(id=identity).first()

    @jwt.token_in_blocklist_loader
    def check_if_token_revoked(jwt_header, jwt_payload):
        jti = jwt_payload["jti"]
        token = TokenBlacklist.objects(jti=jti, revoked=True).first()
        return token is not None
