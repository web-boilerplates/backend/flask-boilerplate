# -*- coding: utf-8 -*-
"""
    server.config
    ~~~~~~~~~~~~~

"""
import os
import logging
from datetime import timedelta


class BaseConfig:
    """Base Configuration"""
    DEBUG = False
    TESTING = False
    LOGGING_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    LOGGING_LOCATION = "flask-base.log"
    LOGGING_LEVEL = logging.DEBUG
    SECRET_KEY = os.getenv("SECRET_KEY")
    JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY")
    BCRYPT_LOG_ROUNDS = 13
    MONGODB_HOST = os.getenv("MONGO_URI", "mongodb://localhost:27017/")
    JWT_TOKEN_LOCATION = ["headers"]
    JWT_BLACKLIST_ENABLED = True
    JWT_ACCESS_TOKEN_EXPIRES_HOURS = 1
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(
        hours=JWT_ACCESS_TOKEN_EXPIRES_HOURS
    )
    JWT_REFRESH_TOKEN_EXPIRES_DAYS = 30
    JWT_REFRESH_TOKEN_EXPIRES = timedelta(
        days=JWT_REFRESH_TOKEN_EXPIRES_DAYS
    )


class DevelopmentConfig(BaseConfig):
    """Development Configuration"""
    DEBUG = True
    BCRYPT_LOG_ROUNDS = 4
    SECRET_KEY = "viva la pluto"
    JWT_SECRET_KEY = "viva la pluto"


class TestingConfig(BaseConfig):
    """Testing Configuration"""
    DEBUG = True
    TESTING = True
    BCRYPT_LOG_ROUNDS = 4


class ProductionConfig(BaseConfig):
    """Production Configuration"""
    DEBUG = False
