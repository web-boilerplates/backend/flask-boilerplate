# -*- coding: utf-8 -*-
"""
    server.api.__init__
    ~~~~~~~~~~~~~~~~~~~

"""
from flask import Blueprint
from server.api.auth import auth_bp


api_bp = Blueprint("api", __name__)

api_bp.register_blueprint(auth_bp)
