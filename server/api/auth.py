# -*- coding: utf-8 -*-
"""
    server.api.auth
    ~~~~~~~~~~~~~~~

"""
from flask import Blueprint, request, current_app as app
from wtforms import Form, StringField, PasswordField, validators
from werkzeug.exceptions import Forbidden
from server.models import (
    User,
    TokenBlacklist
)
from flask_jwt_extended import (
    jwt_required,
    current_user,
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    get_jwt,
    decode_token
)

auth_bp = Blueprint("auth", __name__)


class RegistrationForm(Form):
    username = StringField("Username")
    password = PasswordField("Password", [
        validators.DataRequired(),
        validators.EqualTo("confirm")
    ])
    confirm = PasswordField("Repeat Password")


@auth_bp.post("/auth/register/")
def register():
    form = RegistrationForm(request.form)
    if form.validate():
        User.createOne(
            username=form.username.data,
            password=form.password.data,
            _conflict="A user with that username already exists."
        )

        return {
            "msg": "User Registration Successful"
        }, 201


@auth_bp.post("/auth/login/")
def login():
    username = request.json.get("username")
    password = request.json.get("password")

    user = User.objects(username=username).first()

    if not user or not user.check_password(password):
        raise Forbidden("Invalid username/password!")

    res = {
        "access": create_access_token(identity=user, fresh=True),
        "refresh": create_refresh_token(identity=user)
    }

    decoded_token = decode_token(res["access"])
    TokenBlacklist.createOne(
        jti=decoded_token["jti"],
        user=user
    )

    return res, 200


@auth_bp.post("/auth/refresh/")
@jwt_required(refresh=True)
def refresh():
    identity = get_jwt_identity()

    res = {
        "access_token": create_access_token(identity=identity, fresh=False)
    }

    decoded_token = res["access"]
    TokenBlacklist.createOne(
        jti=decoded_token["jti"],
        user=current_user
    )

    return res, 200


@auth_bp.get("/auth/logout/")
@jwt_required()
def logout():

    jti = get_jwt()["jti"]

    TokenBlacklist.findOne(jti=jti).update(revoked=True)

    res = {
        "msg": "Logout Successful"
    }

    return res, 200
